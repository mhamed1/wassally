const express = require('express');
const path = require('path');
const logger = require('morgan');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');
const categoriesRouter = require('./services/categories/category_router');
const userRouter = require('./services/users/users_router');
const aboutUsRouter = require('./services/aboutUs/aboutUs');
const config = require('./config');
const hooks = require('./middlewares/hooks')
// require('./services/scheduler');
const mongoose = require('mongoose');
const passport = require('passport');

var fs = require('fs');
var staticDir = './static';
var imgDir = './static/images';
var pdfDir = './static/pdf';
if (!fs.existsSync(staticDir)){
  fs.mkdirSync('./static');
}
if (!fs.existsSync(imgDir)){
    fs.mkdirSync('./static/images');
}
if (!fs.existsSync(pdfDir)){
  fs.mkdirSync('./static/pdf');
}

// mongoose.connect(config.DB_CONNECTION.local)
mongoose.connect(config.DB_CONNECTION.mongolab)
  .then(()=>{
    console.log("connected to db successfully");    
  })
  .catch((error) => {
    console.log(error);
    process.exit(1);
});


// const  users = require('./users/users_route');
const authRoute = require('./auth/authentication');
let app = express();
app.use(session({ secret: 'passport-tutorial', cookie: { maxAge: 60000 }, resave: false, saveUninitialized: false }));
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(cors());
app.use(hooks)
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'static')));


// app.use(passport.initialize());
// require('./config/passport');
app.use(passport.initialize());
app.use(passport.session());
require('./config/auth')(passport);

// app.use('/', authRoute);
app.use('/aboutUs',aboutUsRouter);
app.use('/categories',categoriesRouter);
app.use('/',userRouter);


// app.use('/users', users);
app.get('/status', (req, res) => res.send({
  status: "Up and running"
}))
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.send({
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;

// const MailUtil = require('./utils/MailUtils');
// const configMail = require('./config').components.mail;
// const CONSTANTS = require('./utils/constants');
// MailUtil.sendEmail(configMail.SUPPORT.CONFIG, CONSTANTS.MAIL_TEMPLATES.SUPPORT, "Testing Mails", {"some" : "data"}, "elbassel.n13@gmail.com");
