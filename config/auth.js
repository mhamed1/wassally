var path = require('path');

var passport=require('passport');
var User = require('./../services/users/users_schema');

module.exports = function(passport) {

    passport.serializeUser(function(user, done){
        done(null, false);
    });
    passport.deserializeUser(function(id, done){
        console.log("deserializeUser called", id);
        User.findById(id, function (err, user) {
            done(err, user);
        });
    });

    //load strategy files
    require('./strategies/local-strategy')();
    //TODO: Facebook
    //TODO: Twitter
    //TODO: Google
}