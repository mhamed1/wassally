module.exports = {
    createUser: {
        type: 'object',
        properties: {
            username: {
                type: 'string'
            },
            password:{
                type: 'string'
            }
        },
        required: ['username','password']
    },
    userLogin:{
        type: 'object',
        properties: {
            username: {
                type: 'string'
            },
            password:{
                type: 'string'
            }
        },
        required: ['username','password'],
    }
}