const express = require('express');
const router = express.Router();
var fs = require('fs');

router.get('/', (req, res) => {
    fs.readFile(__dirname + '/aboutUs.txt', {}, (err, file) => {
        if (err) {
            res.send(err)
        } else {
            res.send(file)
        }
    });
});

router.post('/', (req, res) => {
    fs.writeFile(__dirname + '/aboutUs.txt', req.body.aboutUs, {}, (err, file) => {
        if (err) {
            res.send('not saved')
        } else {
            res.send('saved successfully')
        }
    });
});


module.exports = router;