const mongoose = require('mongoose');

let categorySchema = mongoose.Schema({

    category: {
        name: {
            type: String
        },
        subCategory:[{
            name:{
                type:String
            },
            items: [{
                name: {
                    type: String
                },
                price:{
                    type:String
                },
                weightUnit:{
                    type:String,
                    enum:['kg','gm']
                },
            }],
        }],
        creationDate: {
            type: Date,
            default: Date.now
        },
        creator: {
            type: String
        }
    },
});

const category = mongoose.model('category', categorySchema)

module.exports = category;