const express = require('express');
const router = express.Router();
const categoryController = require('./category_controller');

router.post('/',categoryController.createcategory);

router.patch('/item',categoryController.addItem);

router.patch('/subCat',categoryController.addSubCategory);

// router.put('/',categoryController.editcategory);

router.get('/',categoryController.retreiveAllcategories);

router.get('/:id',categoryController.retreiveSinglecategory);


module.exports = router;