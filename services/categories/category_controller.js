const renderResponseUtil = require('../../utils/RenderResponseUtil');
const categorieservice = require('./category_service');

module.exports = {
    createcategory,
    retreiveAllcategories,
    retreiveSinglecategory,
    addSubCategory,
    addItem
}

async function addSubCategory(req,res) {
    try {
        const subCatObj = req.body;
        const subCat = await categorieservice.addSubCategory(subCatObj);
        renderResponseUtil.sendResponse(req, res, subCat)
    } catch (error) {
        renderResponseUtil.sendResponse(req, res, error)
    }
}

async function addItem(req,res) {
    try {
        const itemObj = req.body;
        const item = await categorieservice.addItem(itemObj);
        renderResponseUtil.sendResponse(req, res, item)
    } catch (error) {
        renderResponseUtil.sendResponse(req, res, error)
    }
}

// async function editcategory(req, res) {
//     try {
//         const categoryObj = req.body;
//         const user = await categorieservice.editcategory(categoryObj);
//         renderResponseUtil.sendResponse(req, res, user)
//     } catch (error) {
//         renderResponseUtil.sendResponse(req, res, error)
//     }
// }

async function retreiveSinglecategory(req, res) {
    try {
        const category = await categorieservice.retreiveSinglecategory(req.params.id);
        renderResponseUtil.sendResponse(req, res, category)
    } catch (error) {
        renderResponseUtil.sendResponse(req, res, error)
    }
}

async function retreiveAllcategories(req, res) {
    try {
        const categories = await categorieservice.retreiveAllcategories();
        renderResponseUtil.sendResponse(req, res, categories)
    } catch (error) {
        renderResponseUtil.sendResponse(req, res, error)
    }
}

async function createcategory(req, res) {
    try {
        const categoryObj = req.body;
        const company = await categorieservice.createcategory(categoryObj);
        renderResponseUtil.sendResponse(req, res, company)
    } catch (error) {
        renderResponseUtil.sendResponse(req, res, error)
    }
}