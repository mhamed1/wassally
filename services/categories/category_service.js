const categorieschema = require('./category_schema');
const usersSchema = require('../users/users_schema');
const SuccessMessage = require('../../utils/customMessage').SuccessMessage;
const ErrorMessage = require('../../utils/customMessage').ErrorMessage;
const notificationUtil = require('../../utils/notificationUtil');

module.exports = {
  createcategory,
  retreiveSinglecategory,
  retreiveAllcategories,
  addItem,
  addSubCategory,
  getAllCategories,
  getAllSubcategories,
  getAllItems
  // editcategory
};

async function addSubCategory(subCat) {
  try {
    const { query, update } = _formCreateSubCategory(subCat);
    const findExistense = await categorieschema.count({ 'category.subCategory': { $elemMatch: { name: subCat.name } } });
    if (findExistense === 0) {
      const response = await categorieschema.update(query, update);
      const message = new SuccessMessage(SuccessMessage.CREATING_OBJECT_SUCCESS, response);
      return (message);
    } else {
      throw 'This Sub Category is already exists'
    }
  } catch (error) {
    const errorMessage = new ErrorMessage(ErrorMessage.CREATING_OBJECT_ERROR, error);
    throw(errorMessage);
  }


}

async function addItem(item) {
  const { query, update } = _formCreateItems(item)
  const response = await categorieschema.update(query, update);
  const message = new SuccessMessage(SuccessMessage.CREATING_OBJECT_SUCCESS, response);
  return (message);
}



// function editcategory(categoryObj) {
//   return new Promise(async (resolve, reject) => {
//     try {
//       const users = await usersSchema.find({});
//       let notificationDevices = [];
//       users.forEach(element => {
//         notificationDevices.push(...(element.notificationDevices))
//       });
//       notificationDevices = [...new Set(notificationDevices)];

//       const updateParameters = _updateFactory(categoryObj);
//       const response = await categorieschema.findOneAndUpdate(
//         { _id: categoryObj.id }
//         , updateParameters, { new: true });
//       const message = new SuccessMessage(SuccessMessage.UPDATING_OBJECT_SUCCESS, response);
//       resolve(message);
//       notificationUtil.sendNotification('The event category has been changed.'
//         , notificationDevices, { meetingRequestId: response._id })
//     } catch (error) {
//       const errorMessage = new ErrorMessage(ErrorMessage.UPDATING_OBJECT_ERROR, error);
//       reject(errorMessage);
//     }
//   })
// }

function createcategory(categoryObj) {
  return new Promise(async (resolve, reject) => {
    try {
      const category = _formCategory(categoryObj);
      const findExistense = await categorieschema.count({ 'category.name': categoryObj.name });
      if (findExistense === 0) {
        const response = await categorieschema.create(category);
        const message = new SuccessMessage(SuccessMessage.CREATING_OBJECT_SUCCESS, response);
        resolve(message);
      } else {
        throw 'This Category is already exists'
      }
    } catch (error) {
      const errorMessage = new ErrorMessage(ErrorMessage.CREATING_OBJECT_ERROR, error);
      reject(errorMessage);
    }
  })
}



function retreiveSinglecategory(categoryId) {
  return new Promise(async (resolve, reject) => {
    try {
      const category = await categorieschema.find({ _id: categoryId });
      const message = new SuccessMessage(SuccessMessage.GETTING_DATA, category);
      resolve(message);
    } catch (error) {
      const errorMessage = new ErrorMessage(ErrorMessage.FINDING_OBJECT_ERROR, error);
      reject(errorMessage);
    }
  })
}

function retreiveAllcategories() {
  return new Promise(async (resolve, reject) => {
    try {
      const categories = await categorieschema.find({});
      const message = new SuccessMessage(SuccessMessage.GETTING_DATA, categories);
      resolve(message);
    } catch (error) {
      const errorMessage = new ErrorMessage(ErrorMessage.FINDING_OBJECT_ERROR, error);
      reject(errorMessage);
    }
  })
}

function _updateFactory(categoryObj) {
  const updateParams = {
    "day": categoryObj.day,
    "speaker": categoryObj.speaker,
    "place": categoryObj.place,
    "time": categoryObj.time,
    "text": categoryObj.text,
    "activity": categoryObj.activity,
    "from": categoryObj.from,
    "to": categoryObj.to,
  }
  Object.keys(updateParams).forEach((key) => (updateParams[key] == null) && delete updateParams[key]);
  return updateParams;
}


const _formCategory = (categoryObj) => {
  return [
    { category: { name: categoryObj.name, creationDate: Date.now() } }
  ]
}

const _formCreateItems = (item) => {
  const query = { 'category.name': item.category, 'category.subCategory': { $elemMatch: { name: item.subCat } } };
  const update = {
    $addToSet: { 'category.subCategory.$.items': { name: item.name, price: item.price, weightUnit: item.weightUnit } }
  }

  return { query, update };
}

const _formCreateSubCategory = (subCat) => {
  const query = { 'category.name': subCat.category };
  const update = {
    $push: { 'category.subCategory': { name: subCat.name, ...subCat.items && { items: [subCat.items] } } }
  }

  return { query, update };
}