const UserService = require('./users_service');
const renderResponseUtil = require('../../utils/RenderResponseUtil');
const validation = require('./../validationSchemas');
const uploadUtil = require('../../utils/UploadingUtil');
const mailUtil = require('./../../utils/MailUtils')
const SuccessMessage = require('../../utils/customMessage').SuccessMessage;
const ErrorMessage = require('../../utils/customMessage').ErrorMessage;
const passport = require('passport');

module.exports = {
  changePassword,
  createUser,
  userLogin,
  retreiveSingleUser,
  retreiveAllUsers,
  editUser,
  sendMail,
  removeUserDevice,
  addUserDevice,
  editWallet
}

async function editWallet(req,res){
  try {
    const userObj = req.body;
    const user = await UserService.editWallet(req.body);
    renderResponseUtil.sendResponse(req, res, user)
  } catch (error) {
    renderResponseUtil.sendResponse(req, res, error)
  }
}

async function changePassword(req,res){
  try {
    const user = await UserService.changePassword(req.body);
    renderResponseUtil.sendResponse(req, res, user)
  } catch (error) {
    renderResponseUtil.sendResponse(req, res, error)
  }
}

async function addUserDevice(req,res) {
  try {
    const user = await UserService.addUserDevice(req.body);
    renderResponseUtil.sendResponse(req, res, user)
  } catch (error) {
    renderResponseUtil.sendResponse(req, res, error)
  }
}

async function removeUserDevice(req,res) {
  try {
    const user = await UserService.removeUserDevice(req.body);
    renderResponseUtil.sendResponse(req, res, user)
  } catch (error) {
    renderResponseUtil.sendResponse(req, res, error)
  }
}

async function editUser(req, res) {
  try {
    // await uploadUtil.upload(req, res);
    const userObj = req.body;
    const user = await UserService.editUser(userObj);
    renderResponseUtil.sendResponse(req, res, user)
  } catch (error) {
    renderResponseUtil.sendResponse(req, res, error)
  }
}

async function retreiveAllUsers(req, res) {
  try {
    const users = await UserService.retreiveAllUsers();
    renderResponseUtil.sendResponse(req, res, users)
  } catch (error) {
    renderResponseUtil.sendResponse(req, res, error)
  }
}

async function retreiveSingleUser(req, res) {
  try {
    const user = await UserService.retreiveSingleUser(req.params.id);
    renderResponseUtil.sendResponse(req, res, user)
  } catch (error) {
    renderResponseUtil.sendResponse(req, res, error)
  }
}

async function userLogin(req, res,next) {
  try {
    // const userObj = req.body;
    // req.validate(validation.userLogin, userObj);
    // const user = await UserService.userLogin(userObj);
    // console.log("Auth.config", path.join(__dirname, 'strategies', 'local-strategy'))
    passport.authenticate('local', function (err, user, info) {
        if (err || !user) {
            console.log("Error", info);
            return res.status(400).send(info);
        }
 
        req.logIn(user, function(err) {
            if (err) {
               return next(err);
              //  return res.status(404).send("Username or password incorrect");
            }
        })
 
        res.status(200).json(user);
    })(req, res, next);
    // renderResponseUtil.sendResponse(req, res, user)
  } catch (error) {
    renderResponseUtil.sendResponse(req, res, error)
  }
}

async function createUser(req, res) {
  try {
    // await uploadUtil.upload(req, res);
    const userObj = req.body;
    // userObj.files = req.files;
    const user = await UserService.createUser(userObj);
    renderResponseUtil.sendResponse(req, res, user)
  } catch (error) {
    renderResponseUtil.sendResponse(req, res, error)
  }
}

async function sendMail(req,res){
  try {
    const payload = req.body.payload;
    const subject = req.body.subject;
    const sender = req.body.sender;
    const mailRes = await mailUtil.sendEmail(subject, payload , sender)
    const message = new SuccessMessage(SuccessMessage.SENDING_MAIL_SUCCESS, mailRes);
    renderResponseUtil.sendResponse(req, res, message);
  } catch (error) {
    const errorMessage = new ErrorMessage(ErrorMessage.SENDING_MAIL_FAILURE, error);
    renderResponseUtil.sendResponse(req, res, errorMessage)
  }
}