const mongoose = require('mongoose');
const crypto = require('crypto');

let userSchema = mongoose.Schema({
    username: {
      type:String,
      required:true,
      unique:true
    },
    firstname:{
      type:String
    },
    lastname:{
      type:String
    },
    email:{
      type:String,
      unique:true
    },
    phone:{
      type:String,
    },
    password: {
      type:String
    },
    role:{
      type:{
        enum:['USER','ADMIN','GUEST']
      }
    },
    addresses:[{
      name:String
    }],
    favourites:[{
      itemName: {
        type: String
      },
    }],
    orders:[{
      orderId:{
        name:String
      },
      itemName: {
        type: String
      },
      itemId: {
        type: String
      },
      amount : {
        type:Number
      },
      price:{
        type:Number
      }
    }],
    wallet:{
      type:Number
    }
});


userSchema.methods.setPassword = function(password) {
  this.password = createHash(password);
}

userSchema.methods.validatePassword = function(oldPassword , newPassword) {
  return oldPassword === createHash(newPassword);
}

userSchema.methods.createHash = createHash ;

function createHash (password){
  return crypto.pbkdf2Sync(password,'100', 10000, 512, 'sha512').toString('hex');
}
const users = mongoose.model('user',userSchema)

module.exports = users;
