const UserSchema = require('./users_schema');
const SuccessMessage = require('../../utils/customMessage').SuccessMessage;
const ErrorMessage = require('../../utils/customMessage').ErrorMessage;
const _ = require('lodash');

module.exports = {
  changePassword,
  createUser,
  userLogin,
  retreiveSingleUser,
  retreiveAllUsers,
  editUser,
  addUserDevice,
  removeUserDevice,
  editWallet
}

async function changePassword(userObj) {
  try {
    const findUser = await UserSchema.findOne({ '_id': userObj.id });
    const user = new UserSchema();
    const validatePassword = user.validatePassword(findUser.password, userObj.oldPassword);
    if (!validatePassword) {
      throw 'old and new passwords arenot similar'
    }
    await UserSchema.update({ '_id': userObj.id }, { password: user.createHash(userObj.newPassword) })
    return new SuccessMessage(SuccessMessage.UPDATING_OBJECT_SUCCESS, user);

  } catch (error) {
    const errorMessage = new ErrorMessage(ErrorMessage.UPDATING_OBJECT_ERROR, error);
    throw (errorMessage);

  }


}

function addUserDevice(userObj) {
  return new Promise(async (resolve, reject) => {
    try {
      const users = await UserSchema.findOneAndUpdate({ _id: userObj.id }
        , { $addToSet: { notificationDevices: userObj.notificationDevice } }, {
          new: true
        })
      const message = new SuccessMessage(SuccessMessage.UPDATING_OBJECT_SUCCESS, users);
      resolve(message);
    } catch (error) {
      const errorMessage = new ErrorMessage(ErrorMessage.UPDATING_OBJECT_ERROR, error);
      reject(errorMessage);
    }
  })
}

function removeUserDevice(userObj) {
  return new Promise(async (resolve, reject) => {
    try {
      const users = await UserSchema.findOneAndUpdate({ _id: userObj.id }
        , { $pullAll: { notificationDevices: [userObj.notificationDevice] } }, {
          new: true
        })
      const message = new SuccessMessage(SuccessMessage.UPDATING_OBJECT_SUCCESS, users);
      resolve(message);
    } catch (error) {
      const errorMessage = new ErrorMessage(ErrorMessage.UPDATING_OBJECT_ERROR, error);
      reject(errorMessage);
    }
  })
}

function retreiveAllUsers() {
  return new Promise(async (resolve, reject) => {
    try {
      const users = await UserSchema.find({});
      const message = new SuccessMessage(SuccessMessage.GETTING_DATA, users);
      resolve(message);
    } catch (error) {
      const errorMessage = new ErrorMessage(ErrorMessage.FINDING_OBJECT_ERROR, error);
      reject(errorMessage);
    }
  })
}

function retreiveSingleUser(userId) {
  return new Promise(async (resolve, reject) => {
    try {
      const user = await UserSchema.find({
        _id: userId
      })
      const message = new SuccessMessage(SuccessMessage.GETTING_DATA, user);
      resolve(message);
    } catch (error) {
      const errorMessage = new ErrorMessage(ErrorMessage.FINDING_OBJECT_ERROR, error);
      reject(errorMessage);
    }
  })
}

function userLogin(userObj) {
  return new Promise(async (resolve, reject) => {
    UserSchema.findOne({
      username: userObj.username,
      // password: userObj.password
    })
      .then((foundedUser) => {
        if (foundedUser) {
          const message = new SuccessMessage(SuccessMessage.AUTHENTICATION_SUCCESS, {
            user: foundedUser
          });
          resolve(message);
        } else {
          const errorMessage = new ErrorMessage(ErrorMessage.INVALID_USERNAME_PASSWORD, 'invalid user name or password');
          reject(errorMessage);
        }
      })
      .catch((error) => {
        const errorMessage = new ErrorMessage(ErrorMessage.FINDING_OBJECT_ERROR, error);
        reject(errorMessage);
      });
  })
}


function createUser(userObj) {
  return new Promise(async (resolve, reject) => {
    try {
      const finalUser = new UserSchema(userObj);
      finalUser.setPassword(userObj.password);

      let response = await finalUser.save();
      response = _.pick(response, __signUpPickUP(response));
      const message = new SuccessMessage(SuccessMessage.CREATING_OBJECT_SUCCESS, response);
      resolve(message);
    } catch (error) {
      const errorMessage = new ErrorMessage(ErrorMessage.CREATING_OBJECT_ERROR, error);
      reject(errorMessage);
    }
  })
}


async function editWallet(userObj) {
  try {
    const response = await UserSchema.findOneAndUpdate({
      _id: userObj.id
    }, {$set:{wallet: userObj.walletBalance}}, {
        new: true
      });
    const message = new SuccessMessage(SuccessMessage.UPDATING_OBJECT_SUCCESS, response);
    return (message);
  } catch (error) {
    const errorMessage = new ErrorMessage(ErrorMessage.UPDATING_OBJECT_ERROR, error);
    throw (errorMessage);
  }
}

async function editUser(userObj) {
  try {
    const updateParameters = _updateFactory(userObj);
    const response = await UserSchema.findOneAndUpdate({
      _id: userObj.id
    }, updateParameters, {
        new: true
      });
    const message = new SuccessMessage(SuccessMessage.UPDATING_OBJECT_SUCCESS, response);
    return (message);
  } catch (error) {
    const errorMessage = new ErrorMessage(ErrorMessage.UPDATING_OBJECT_ERROR, error);
    throw (errorMessage);
  }
}


function _updateFactory(userObj) {
  const updateParams = {
    "username": userObj.username,
    "firstname": userObj.firstname,
    "lastname": userObj.lastname,
    "phone": userObj.mobile,
    "title": userObj.title,
    "wallet": userObj.wallet
  }

  Object.keys(updateParams).forEach((key) => (updateParams[key] == null) && delete updateParams[key]);
  return updateParams;
}


const __signUpPickUP = (response) => {
  return ['firstname', 'lastname', 'email', 'username', 'email', 'phone' , '_id']
}