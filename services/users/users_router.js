const express = require('express');
const router = express.Router();
const userController = require('./users_controller');
router.get('/status',(req,res)=>{
    res.send('App is running ...')
})

router.post('/user',userController.createUser);

router.put('/user/password',userController.changePassword)

// router.post('/logout',userController.removeUserDevice);

// router.post('/device',userController.addUserDevice);

router.get('/user/:id',userController.retreiveSingleUser);

router.get('/user',userController.retreiveAllUsers);

router.put('/user',userController.editUser);

router.put('/user/wallet',userController.editWallet);

router.post('/login',userController.userLogin);

router.post('/contact',userController.sendMail);

module.exports = router;