const multer = require('multer');
const path = require('path');
const pify = require('pify');
const directoryPath = path.resolve(__dirname, '../');
const shortid = require('shortid');

const storage = multer.diskStorage({
    destination: function (req, file, callback) {        
        switch (file.fieldname) {
            case 'image':
                callback(null, directoryPath + '/static/images');
                break;
            case 'pdf':
                callback(null, directoryPath + '/static/pdf');
                break;    
            default:
                break;
        }
    },
    filename: function (req, file, callback) {
        callback(null, shortid.generate()+path.extname(file.originalname));
    }
});

let fileFilter = (req, file, cb)=>{
        var ext = path.extname(file.originalname);
        if((file.fieldname=='image') && !(ext == '.png' || ext == '.jpg' || ext == '.gif' || ext == '.jpeg')) {
            return cb("only image file is allowed")
        }
        if((file.fieldname=='pdf') && !(ext == '.pdf')) {
            return cb("only pdf file is allowed")
        }
        cb(null, true)
};

var upload = pify(multer({
    fileFilter : fileFilter,
    storage: storage
}).fields([{ name: 'image', maxCount: 1 }, { name: 'pdf', maxCount: 1 }]))

module.exports = {
    upload,
};