const ONE_SIGNAL_APP_KEY = "055a6b18-ebd0-4b4e-88f1-fab6c10eacf3";
const ONE_SIGNAL_APIS_KEY = "YTIwZTQ0MDctOTNiOC00MzQ5LWIyMWQtZjNiOTQ5MGRlMWIy";
var onesignal = require('node-opensignal-api');
var onesignal_client = onesignal.createClient();

module.exports = {
    sendNotification
}

function sendNotification(content, player_id, info) {
    var params = {
        app_id: ONE_SIGNAL_APP_KEY,
        contents: {
            'en': content
        },
        data: info,
        include_player_ids: player_id
    };
    onesignal_client.notifications.create(ONE_SIGNAL_APIS_KEY, params, function (err, response) {
        if (err) {
            console.log('Encountered error', err);
            return false;
        } else {
            console.log(response);
            return true;
        }
    });
  }

// var params = {
//   app_id: ONE_SIGNAL_APP_KEY,
//   contents: {
//       'en': "welcome to event App "
//   },
//   data: { "datakey": "dataVal" },
//   include_player_ids: ['acf3e726-4780-49c5-9a8e-0a3cba11b46b','8ca57ce6-6b70-4f3f-864c-61a497267867','030f00ae-d8d8-4055-9c7c-20b01dc8ca20']
// };


