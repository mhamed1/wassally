const Ajv = require('ajv');
const ErrorMessage = require('./customMessage').ErrorMessage;
const renderResponseUtil = require('./RenderResponseUtil');

module.exports = (req, res, next) => {
    req.validate = (schema, data) => {
        const ajv = new Ajv({allErrors:true});
        const valid = ajv.validate(schema, data);        
        if (!valid) {
            const errorMessage = new ErrorMessage(ErrorMessage.VALIDATING_OBJECT_ERROR, ajv.errors);
            renderResponseUtil.sendResponse(req,res,errorMessage)
        }
    };
    next();
};
