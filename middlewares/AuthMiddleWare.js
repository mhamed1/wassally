const moment = require('moment');
const jwt = require('jwt-simple');
// const _ = require('lodash');
const config = require('../../config/config');
// const UserService = require('../../business-modules/users/users_service');
/*
 |--------------------------------------------------------------------------
 | Generate JSON Web Token
 |--------------------------------------------------------------------------
 */
const jwtgen = function createJWT(user) {
  const payload = {
    data: user,
    iat: moment().format('YYYY-MM-DD'),
    exp: moment().add(1, 'months').format('YYYY-MM-DD'),
  };
  return jwt.encode(payload, config.TOKEN_SECRET);
};

const authMiddleware = function (roles) {
  return function ensureAuthenticated(req, res, next) {
    if (!req.header('Authorization')) {
      return res.status(401).send({ message: 'Please make sure your request has an Authorization header' });
    }
    const token = req.header('Authorization').split(' ')[1];
    let payload = null;
    try {
      payload = jwt.decode(token, config.TOKEN_SECRET);
    } catch (err) {
      return res.status(401).send({ message: err.message });
    }
    if (payload.exp <= moment().format('YYYY-MM-DD')) {
      return res.status(401).send({ message: 'Token has expired' });
    }

    req.user = payload.data;
    if (moment(payload.iat).format('YYYY-MM-DD') != moment().format('YYYY-MM-DD')) {
      let token = jwtgen(req.user);
      UserService.addUserToUsersLoginsLogs(req.user.id)
        .then()
        .catch(console.log)
      res.setHeader('Authorization', `JWT ${token}`)
    }
    if (roles[0]) {
      const flag = _.some(roles, role => req.user.type_of_account === role);
      if (flag) {
        next();
      } else {
        res.send(403);
      }
    } else {
      next();
    }
  };
};


module.exports.authMiddleware = authMiddleware;
module.exports.jwtgen = jwtgen;
